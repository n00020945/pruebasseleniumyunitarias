﻿using System;
using System.Linq;
using CalidadT2.Models;
using CalidadT2.Repository;
using CalidadT2.Service;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace CalidadT2.Controllers
{
    public class LibroController : Controller
    {
        private readonly ICookieAuthService _cookieAuthService;
        private readonly ILibroRepository _libro;
        
        private readonly IUsuarioRepository _usuario;

        public LibroController(ILibroRepository _libro,ICookieAuthService _cookieAuthService,IUsuarioRepository _usuario
        )
        {
            this._usuario = _usuario;
       
            this._libro = _libro;
            this._cookieAuthService = _cookieAuthService;

        }
        

        [HttpGet]
        public IActionResult Details(int id)
        {
            var model = _libro.Detalle(id);
            return View(model);
        }

        [HttpPost]
        public IActionResult AddComentario(Comentario comentario)
        {
            Usuario user = LoggedUser();
            
           
            _libro.AgregarComentario(comentario,user);

            return RedirectToAction("Details", new { id = comentario.LibroId });
        }

        private Usuario LoggedUser()
        {
            
            _cookieAuthService.SetHttpContext(HttpContext);
            var claim = _cookieAuthService.ObetenerClaim();
            var user = _usuario.UsuarioLogeado(claim);
            return user;
        }
    }
}

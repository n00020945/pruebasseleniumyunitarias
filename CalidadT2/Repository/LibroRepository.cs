using System;
using System.Collections.Generic;
using System.Linq;
using CalidadT2.Models;
using Microsoft.EntityFrameworkCore;

namespace CalidadT2.Repository
{
    public interface ILibroRepository
    {
        public Libro Detalle(int Id);

        public List<Libro> TodosLOsLibros();

        public void AgregarComentario(Comentario comentario,Usuario user);

        
    }
    
    public class LibroRepository : ILibroRepository
    {
        private AppBibliotecaContext context;

        public LibroRepository(AppBibliotecaContext context)
        {
            this.context = context;
        }
        
        public Libro Detalle(int id)
        {
           var detalleLibro= context.Libros
               .Include("Autor")
               .Include("Comentarios.Usuario")
               .FirstOrDefault(o => o.Id == id);
           return detalleLibro;
        }

        public List<Libro> TodosLOsLibros()
        {
            var model = context.Libros.Include(o => o.Autor).ToList();
            return model;
        }

        public void AgregarComentario(Comentario comentario,Usuario user)
        {
            comentario.UsuarioId = user.Id;
            comentario.Fecha = DateTime.Now;
            context.Comentarios.Add(comentario);

            var libro = context.Libros.Where(o => o.Id == comentario.LibroId).FirstOrDefault();
            libro.Puntaje = (libro.Puntaje + comentario.Puntaje) / 2;

            context.SaveChanges();
        }
    }
}
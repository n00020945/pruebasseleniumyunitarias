using System.Collections.Generic;
using System.Linq;
using CalidadT2.Constantes;
using CalidadT2.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace CalidadT2.Repository
{
    public interface IBibliotecaRepository
    {
        public void MarcarComoTerminada(Usuario user,int libroId);
        public void MarcarComoLeyendo(Usuario user,int libroId);
        
        public void Agregar(Usuario user,int libro);
       
        public List<Biblioteca> MiBiblioteca(Usuario user);
        
        
    }
    public class BibliotecaRepository : IBibliotecaRepository
    {
        private AppBibliotecaContext context;

        public BibliotecaRepository(AppBibliotecaContext context)
        {
            this.context = context;
        }
        public void MarcarComoTerminada(Usuario user,int libroId)
        {
           var libro= context.Bibliotecas
               .FirstOrDefault(o => o.LibroId == libroId && o.UsuarioId == user.Id);

            libro.Estado = ESTADO.TERMINADO;
            context.SaveChanges();
        }

        public void MarcarComoLeyendo(Usuario user, int libroId)
        {
            var libro= context.Bibliotecas
                .FirstOrDefault(o => o.LibroId == libroId && o.UsuarioId == user.Id);

            libro.Estado = ESTADO.LEYENDO;
            context.SaveChanges();
        }

        public void Agregar(Usuario user, int libro)
        {
            var biblioteca = new Biblioteca
            {
                LibroId = libro,
                UsuarioId = user.Id,
                Estado = ESTADO.POR_LEER
            };

            context.Bibliotecas.Add(biblioteca);
            context.SaveChanges();
        }

        public List<Biblioteca> MiBiblioteca(Usuario user)
        {
            var model = context.Bibliotecas
                .Include(o => o.Libro.Autor)
                .Include(o => o.Usuario)
                .Where(o => o.UsuarioId == user.Id)
                .ToList();
            return model;
        }
    }
}
using System;
using CalidadT2.Controllers;
using CalidadT2.Models;
using CalidadT2.Repository;
using CalidadT2.Service;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Moq;
using NUnit.Framework;

namespace CalidadTestUnit.TestControladores
{
    public class AuthControllerTest
    {
        [Test]
        public void LoginRetornRedirecToAction()
        {                        
            var repositoryMock = new Mock<IUsuarioRepository>();
            repositoryMock.Setup(o => o.EncontrarUsuario("admin", "admin")).Returns(new Usuario { });

            var authMock = new Mock<ICookieAuthService>();
    

            var controller = new AuthController(repositoryMock.Object, authMock.Object);
            var result = controller.Login("admin", "admin");

            Assert.IsInstanceOf<RedirectToActionResult>(result);
        }
        
        [Test]
        public void LoginRetornView()
        {                        
            var repositoryMock = new Mock<IUsuarioRepository>();
            

            var authMock = new Mock<ICookieAuthService>();
    

            var controller = new AuthController(repositoryMock.Object, authMock.Object);
            var result = controller.Login("admin", "admin");

            Assert.IsInstanceOf<ViewResult>(result);
        }
    }
}
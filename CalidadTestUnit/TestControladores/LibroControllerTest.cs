using System;
using System.Security.Claims;
using CalidadT2.Controllers;
using CalidadT2.Models;
using CalidadT2.Repository;
using CalidadT2.Service;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Moq;
using NUnit.Framework;

namespace CalidadTestUnit.TestControladores
{
    public class LibroControllerTest
    {
        [Test]
        public void DetailsReturnView()
        {
            
            var libroRepositoryMock = new Mock<ILibroRepository>();
            libroRepositoryMock.Setup(o => o.Detalle(2)).Returns(new Libro());
            var controller = new LibroController(libroRepositoryMock.Object,null,null);
            var view =controller.Details(2);
            Assert.IsInstanceOf<ViewResult>(view);
        }
        
        [Test]
        public void AgregarComentarioRetornREdirectToAction()
        {
            var CookieAuthService = new Mock<ICookieAuthService>();
            var UsuarioRepositoryMock = new Mock<IUsuarioRepository>();
            
            Comentario nuevo = new Comentario();
            nuevo.Fecha=DateTime.Now;
            nuevo.Puntaje = 4;
            nuevo.Texto = "sdsadsd";
            Usuario usuario = new Usuario();
            usuario.Id = 1;
            usuario.Nombres = "sadsd";
            usuario.Password = "dsadasd";
            usuario.Username = "sadasd";
            UsuarioRepositoryMock.Setup(o => o.UsuarioLogeado(null)).Returns(usuario);
            var libroRepositoryMock = new Mock<ILibroRepository>();
           var controladorLibro = new LibroController(libroRepositoryMock.Object,CookieAuthService.Object,UsuarioRepositoryMock.Object);
           var view =controladorLibro.AddComentario(nuevo);
            
            Assert.IsInstanceOf<RedirectToActionResult>(view);
        }
        
        
      
    }
}
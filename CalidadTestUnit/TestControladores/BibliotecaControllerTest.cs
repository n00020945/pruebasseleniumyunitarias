using System;
using System.Collections.Generic;
using CalidadT2.Controllers;
using CalidadT2.Models;
using CalidadT2.Repository;
using CalidadT2.Service;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Moq;
using NUnit.Framework;

namespace CalidadTestUnit.TestControladores
{
    public class BibliotecaControllerTest
    {
        [Test]
        public void IndexRetrunView()
        {
            var CookieAuthService = new Mock<ICookieAuthService>();
            var UsuarioRepositoryMock = new Mock<IUsuarioRepository>();
            
            Comentario nuevo = new Comentario();
            nuevo.Fecha=DateTime.Now;
            nuevo.Puntaje = 4;
            nuevo.Texto = "sdsadsd";
            Usuario usuario = new Usuario();
            usuario.Id = 1;
            usuario.Nombres = "sadsd";
            usuario.Password = "dsadasd";
            usuario.Username = "sadasd";
            UsuarioRepositoryMock.Setup(o => o.UsuarioLogeado(null)).Returns(usuario);
            List<Biblioteca> milista = new List<Biblioteca>();
            var bibliotecaRepositoryMock = new Mock<IBibliotecaRepository>();
            bibliotecaRepositoryMock.Setup(o => o.MiBiblioteca(null)).Returns(milista);
            var controller = new BibliotecaController(bibliotecaRepositoryMock.Object,CookieAuthService.Object,UsuarioRepositoryMock.Object);
            var view =controller.Index();
            Assert.IsInstanceOf<ViewResult>(view);
        }
        
        [Test]
        public void AddRetornRedirectToAction()
        {
            var CookieAuthService = new Mock<ICookieAuthService>();
            var UsuarioRepositoryMock = new Mock<IUsuarioRepository>();
            
            Comentario nuevo = new Comentario();
            nuevo.Fecha=DateTime.Now;
            nuevo.Puntaje = 4;
            nuevo.Texto = "sdsadsd";
            Usuario usuario = new Usuario();
            usuario.Id = 1;
            usuario.Nombres = "sadsd";
            usuario.Password = "dsadasd";
            usuario.Username = "sadasd";
            UsuarioRepositoryMock.Setup(o => o.UsuarioLogeado(null)).Returns(usuario);
            List<Biblioteca> milista = new List<Biblioteca>();
            var bibliotecaRepositoryMock = new Mock<IBibliotecaRepository>();
            bibliotecaRepositoryMock.Setup(o => o.Agregar(usuario,2));
            var controller = new BibliotecaController(bibliotecaRepositoryMock.Object,CookieAuthService.Object,UsuarioRepositoryMock.Object);
            
            var view =controller.Add(2);
            Assert.IsInstanceOf<RedirectToActionResult>(view);
        }
        
        [Test]
        public void MarcaComoLeyendo()
        {
            var CookieAuthService = new Mock<ICookieAuthService>();
            var UsuarioRepositoryMock = new Mock<IUsuarioRepository>();
            
            Comentario nuevo = new Comentario();
            nuevo.Fecha=DateTime.Now;
            nuevo.Puntaje = 4;
            nuevo.Texto = "sdsadsd";
            Usuario usuario = new Usuario();
            usuario.Id = 1;
            usuario.Nombres = "sadsd";
            usuario.Password = "dsadasd";
            usuario.Username = "sadasd";
            UsuarioRepositoryMock.Setup(o => o.UsuarioLogeado(null)).Returns(usuario);
            List<Biblioteca> milista = new List<Biblioteca>();
            var bibliotecaRepositoryMock = new Mock<IBibliotecaRepository>();
            bibliotecaRepositoryMock.Setup(o => o.Agregar(usuario,2));
            var controller = new BibliotecaController(bibliotecaRepositoryMock.Object,CookieAuthService.Object,UsuarioRepositoryMock.Object);
            
            var view =controller.MarcarComoLeyendo(2);
            Assert.IsInstanceOf<RedirectToActionResult>(view);
        }
        
        [Test]
        public void MarcaComoTerminado()
        {
            var CookieAuthService = new Mock<ICookieAuthService>();
            var UsuarioRepositoryMock = new Mock<IUsuarioRepository>();
            
            Comentario nuevo = new Comentario();
            nuevo.Fecha=DateTime.Now;
            nuevo.Puntaje = 4;
            nuevo.Texto = "sdsadsd";
            Usuario usuario = new Usuario();
            usuario.Id = 1;
            usuario.Nombres = "sadsd";
            usuario.Password = "dsadasd";
            usuario.Username = "sadasd";
            UsuarioRepositoryMock.Setup(o => o.UsuarioLogeado(null)).Returns(usuario);
            List<Biblioteca> milista = new List<Biblioteca>();
            var bibliotecaRepositoryMock = new Mock<IBibliotecaRepository>();
            bibliotecaRepositoryMock.Setup(o => o.Agregar(usuario,2));
            var controller = new BibliotecaController(bibliotecaRepositoryMock.Object,CookieAuthService.Object,UsuarioRepositoryMock.Object);
            
            var view =controller.MarcarComoTerminado(2);
            Assert.IsInstanceOf<RedirectToActionResult>(view);
        }
        
    }
}
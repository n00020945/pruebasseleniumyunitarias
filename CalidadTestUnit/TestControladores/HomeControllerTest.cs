using CalidadT2.Controllers;
using CalidadT2.Models;
using CalidadT2.Repository;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;

namespace CalidadTestUnit.TestControladores
{
    public class HomeControllerTest
    {
        [Test]
        public void DetailsReturnView()
        {
            var libroRepositoryMock = new Mock<ILibroRepository>();
            libroRepositoryMock.Setup(o => o.TodosLOsLibros());
            var controller = new HomeController(null,libroRepositoryMock.Object);
            var view =controller.Index();
            Assert.IsInstanceOf<ViewResult>(view);
        }
    }
}